/**
 * @file
 * Javascript for the Geofield GEO Software Map formatter.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * The Geofield GEO Software Map formatter behavior.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the Geofield GEO Software Map formatter behavior.
   */
  Drupal.behaviors.geofieldGeoSoftwareMap = {
    attach: function (context, drupalSettings) {

      function loadMap(mapId) {
        // Check if the Map container really exists and hasn't been yet
        // initialized.
        if (drupalSettings['geofield_geosoftware_map'][mapId]) {

          let map_settings = drupalSettings['geofield_geosoftware_map'][mapId]['map_settings'];
          let data = drupalSettings['geofield_geosoftware_map'][mapId]['data'];

          // Set the map_data[mapid] settings.
          Drupal.geoFieldMapGeoFormatter.map_data[mapId] = map_settings;

          // Load the GEO API Library, if needed.
          Drupal.geoFieldMapGeoFormatter.loadGeoAPI(mapId, map_settings.geo_settings.api_version, function () {
            if (!document.getElementById(mapId)) {
              return;
            }
            Drupal.geoFieldMapGeoFormatter.map_geo_initialize(mapId, map_settings, data, context);
          });
        }
      }

      if (drupalSettings['geofield_geosoftware_map']) {

        // If the IntersectionObserver API is available, create an observer to load the map when it enters the viewport
        // It will be used to handle map loading instead of displaying the map on page load.
        let mapObserver = null;
        if ('IntersectionObserver' in window) {
          mapObserver = new IntersectionObserver(function (entries, observer) {
            for (let i = 0; i < entries.length; i++) {
              if (entries[i].isIntersecting) {
                once('geofield-map-loaded', entries[i].target).forEach(function (el) {
                  loadMap(el.id);
                });
              }
            }
          });
        }

        once('geofield-processed', '.geofield-geosoftware-map', context).forEach(function (element) {
          const mapId = $(element).attr('id');
          if (drupalSettings['geofield_geosoftware_map'][mapId]) {
            const map_settings = drupalSettings['geofield_geosoftware_map'][mapId]['map_settings'];
            if (mapObserver && map_settings['map_lazy_load'] && map_settings['map_lazy_load']['lazy_load']) {
              mapObserver.observe(element);
            } else {
              loadMap(mapId);
            }
          }
        });
      }
    }
  };

  Drupal.geoFieldMapGeoFormatter = {

    map_start: {
      center: {lat: 41.85, lng: -87.65},
      zoom: 18
    },

    map_data: {},

    // GEO Software Maps are loaded lazily.
    // This flag will prevent repeat $.getScript() calls.
    maps_api_loading: false,

    /**
     * Provides the callback that is called when maps loads.
     */
    mapCallback: function () {
      let self = this;
      // Wait until the window load event to try to use the maps library.
      $(document).ready(function (e) {
        self.geoCallbacks.forEach(function (callback) {
          callback.callback();
        });
        self.geoCallbacks = [];
      });
    },

    /**
     * Adds a callback that will be called once the maps library is loaded.
     *
     * @param {string} callback - The callback
     */
    addCallback: function (callback) {
      let self = this;
      // Ensure callbacks array.
      self.geoCallbacks = self.geoCallbacks || [];
      self.geoCallbacks.push({callback: callback});
    },

    /**
     * Load GEO API
     */
    loadGeoAPI: function (mapid, geo_api_version, callback) {
      var self = this;

      // Add the callback.
      self.addCallback(callback);

      if (typeof geo === 'undefined') {
        if (self.maps_api_loading === true) {
          return;
        }
        self.maps_api_loading = true;

        geo_api_version = geo_api_version || 'last';
        var scriptPath = 'https://geoapi.business-geografic.com/api/' + geo_api_version + '/';
        console.log("Loading GEO API Script", scriptPath);
        // cache GEO API script
        // https://gist.github.com/steveosoule/628430dacde21fd766fe8c4e796dad94
        $.getScript({url: scriptPath, cache: true})
          .done(function () {
            self.maps_api_loading = false;
            self.mapCallback();
          }).fail(function () {
          $("#" + mapid).html("Could not load GEO API with URL '" + scriptPath + "'");
        });
      } else {
        // API loaded. Run callback.
        self.mapCallback();
      }
    },

    map_geo_initialize: function (mapid, map_settings, data, context) {
      console.log("map_geo_initialize", arguments);
      var urlApp = map_settings.geo_settings.application_url;
      var center = {
        crs: 4326
      };
      if (map_settings.map_center) {
        center.y = parseFloat(map_settings.map_center.lat);
        center.x = parseFloat(map_settings.map_center.lon);
      } else {
        // centre de la france
        center.y = 47.1;
        center.x = 2.5;
      }

      var options = {
        elementId: mapid,
        applicationId: urlApp,
        map: {
          markers: []
        },
        components: { // visibilité des composants
          // COMMON
          Header: {visible: false},
          UserInfoHeader: {visible: false},
          BaseLayerSwitcher: {visible: false},

          // GP
          LayerControl: {visible: false},
          PrintLink: {visible: false},
          SidePanel: {visible: false},
          // PRO
          MapIdentifierTool: {visible: false},
          LayerQueryTool: {visible: false},
          RightPanel: {visible: false},
          FunctionsLauncher: {visible: false}
        }
      };
      var map_controls = map_settings.map_controls;
      if (map_controls) {
        options.components.MapScaleLine = { visible: parseInt(map_controls.scale_control, 10) == 1 };
        options.components.MapBasicControls = { visible: parseInt(map_controls.zoom_control, 10) == 1 };
        options.components.MapGeolocation = { visible: parseInt(map_controls.geolocation_control, 10) == 1 };
      }

      var zoom = map_settings.map_zoom_and_pan.zoom.initial ? parseInt(map_settings.map_zoom_and_pan.zoom.initial) : 8;

      // Set the zoom force and center property for the map.
      var zoom_force = !!map_settings.map_zoom_and_pan.zoom.force;
      var center_force = !!map_settings.map_center.center_force;

      // remplace les liens relatifs en absolus + ouverture dans la page "parent"
      var replaceHTMLContent = function (content) {
        if (content == null || content == "") {
          return null;
        }
        var dom = $('<div>' + content + '</div>');
        dom.find("a").attr("target", '_parent');
        dom.find("a[href^='/']").prop("href",
          function (_idx, oldHref) {
            return oldHref;
          }
        );
        return dom.html();
      };

      // affichage des objets passés à la carte (GeoJSON)
      var markers = [];
      if (data) {
        var features = data.features;
        if (features && features.length > 0 && (features.type !== 'Error')) {
          var extent = {
            minX: Number.POSITIVE_INFINITY,
            maxX: Number.NEGATIVE_INFINITY,
            minY: Number.POSITIVE_INFINITY,
            maxY: Number.NEGATIVE_INFINITY,
            crs: 4326
          };
          var i = 0;
          features.forEach(function (feature) {
            var coordinates = feature.geometry.coordinates;
            if (extent.minX === undefined) {
              extent.minX = coordinates[0];
              extent.minY = coordinates[1];
              extent.maxX = coordinates[0];
              extent.maxY = coordinates[1];
            } else {
              extent.minX = Math.min(extent.minX, coordinates[0]);
              extent.minY = Math.min(extent.minY, coordinates[1]);
              extent.maxX = Math.max(extent.maxX, coordinates[0]);
              extent.maxY = Math.max(extent.maxY, coordinates[1]);
            }
            var marker = {
              position: {
                coordinates: coordinates,
                crs: 4326,
              },
              id: feature.properties.entity_id,
              imageUrl: "public/canvas/images/marker-icon.png",
              positioning: 'bottom-center',
              tooltip: {
                title: replaceHTMLContent(feature.properties.tooltip),
                content: replaceHTMLContent(feature.properties.description)
                //width: 100
              },
              size: {
                w: map_settings.map_marker_and_infowindow.icon_image_width || 25,
                h: map_settings.map_marker_and_infowindow.icon_image_height || 41
              }

            };
            if (map_settings.map_marker_and_infowindow.icon_image_path && map_settings.map_marker_and_infowindow.icon_image_path != "") {
              marker.imageUrl = map_settings.map_marker_and_infowindow.icon_image_path;
            }
            if (i == 0 && map_settings.map_marker_and_infowindow.force_open == 1) {
              marker.tooltip.open = true;
            }
            markers.push(marker);
            i++;
          });

          if (zoom_force || (i === 1)) {
            center.x = extent.minX + ((extent.maxX - extent.minX) / 2);
            center.y = extent.minY + ((extent.maxY - extent.minY) / 2);
          } else {
            var zoom_finer = map_settings.map_zoom_and_pan.zoom.finer ? parseInt(map_settings.map_zoom_and_pan.zoom.finer) : 0;
            if (center_force) {
              zoom += zoom_finer;
            } else {
              var buffer_x = (extent.maxX - extent.minX) / 2;
              var buffer_y = (extent.maxY - extent.minY) / 2;
              var delta_buffer = Math.max(buffer_x, buffer_y);
              if (zoom_finer !== 0) {
                // In case of map initial position not forced, and zooFiner not
                // null/neutral, adapt the Map Zoom and the Start Zoom accordingly.
                if (zoom_finer > 0) {
                  for (i = 1; i < zoom_finer; i++) {
                    delta_buffer *= 2;
                  }
                }
                else {
                  for (i = -1; i > zoom_finer; i--) {
                    delta_buffer += delta_buffer / 2;
                  }
                  delta_buffer *= -1;
                }
                extent.minX -= delta_buffer;
                extent.maxX += delta_buffer;
                extent.minY -= delta_buffer;
                extent.maxY += delta_buffer;
              } else {
                // On ajoute 10% de marge supplémentaire
                extent.minX -= delta_buffer * 0.10;
                extent.maxX += delta_buffer * 0.10;
                extent.minY -= delta_buffer * 0.10;
                extent.maxY += delta_buffer * 0.10;
              }
              options.map.extent = extent;
            }
          }
        }
      }
      if (!options.map.extent) {
        options.map.position = {
          coordinates: [center.x, center.y],
          crs: 4326,
        };
        options.map.scaleDenominator = 2 ** (29 - zoom);
      }
      var markerClusteringEnabled =
        map_settings.map_markercluster && map_settings.map_markercluster.markercluster_control == 1;
      if (!markerClusteringEnabled) {
        options.map.markers = markers;
      }
      // Add map_additional_options if any.
      if (map_settings.map_additional_options.length > 0) {
        var additionalOptions = JSON.parse(map_settings.map_additional_options);
        for (var key in additionalOptions) {
          var additionalOptionsValue = additionalOptions[key];
          if (options.hasOwnProperty(key)) {
            $.extend(options[key], additionalOptionsValue);
          }
          else {
            options[key] = additionalOptionsValue;
          }
        }
      }
      console.log("GEO Map creation options", options);
      var geoApp = new geo.Application(options);
      geoApp.render();
      geoApp.on("initialized", function () {
        var pingTest = 0;
        if (markerClusteringEnabled) {
          // Add markercluster_additional_options if any.
          var markerClusterOptions = {};
          if (map_settings.map_markercluster.markercluster_additional_options.length > 0) {
            var markeclusterAdditionalOptions = JSON.parse(map_settings.map_markercluster.markercluster_additional_options);
            // Merge markerClusterOptions with markeclusterAdditionalOptions.
            $.extend(markerClusterOptions, markeclusterAdditionalOptions);
          }
          var markercluster_display_type = map_settings.map_markercluster.markercluster_display_type || 'circle';
          markerClusterOptions.displayType = markercluster_display_type;

          var addClusteredMarkers = function () {
            geoApp.send("ADD_CLUSTERED_MARKERS", {markers: markers, options: markerClusterOptions});
          };
          var onClusteredMarkersPingSuccess = function () {
            addClusteredMarkers();
          };

          var onClusteredMarkersPingError = function () {
            pingTest++;
            setTimeout(testClusteredMarkers, 100);
            if (pingTest > 100 && pingTest % 10 == 0) {
              console.warn("GEO Clusters: The GEO Clustering script could not be found");
            }
          };
          // test if the script is present, if not, retry until it is
          var testClusteredMarkers = function () {
            geoApp.send("ADD_CLUSTERED_MARKERS_PING").then(onClusteredMarkersPingSuccess, onClusteredMarkersPingError);
          };
          testClusteredMarkers();
        }

        console.log("GEO map initialized");
      });
      // set markers list anyway in options, even if clustered, so that an external script can fetch the list
      options.markers = markers;
      // associate options with the map
      geoApp.options = options;
      window.geofieldGeosoftwareMaps = window.geofieldGeosoftwareMaps || {};
      window.geofieldGeosoftwareMaps[mapid] = geoApp;

      $(context).trigger('geofieldMapInit', mapid);
    }

  };

})(jQuery, Drupal);
