/**
 * @file
 * Javascript for the Geofield Map widget.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  if (!Drupal.geoFieldMap.ext) {
    Drupal.geoFieldMap.ext = {};
  }
  Drupal.geoFieldMap.ext.geo = {

    map_data: Drupal.geoFieldMap.map_data,

    // GEO Maps are loaded lazily. In some situations load_geo() is called twice which may cause unexpected errors.
    // This flag will prevent repeat $.getScript() calls.
    maps_api_loading: false,

    /**
     * Provides the callback that is called when maps loads.
     */
    mapCallback: function () {
      let self = this;
      // Wait until the window load event to try to use the maps library.
      $(document).ready(function (e) {
        self.geoCallbacks.forEach(function (callback) {
          callback.callback(e);
        });
        self.geoCallbacks = [];
      });
    },

    /**
     * Adds a callback that will be called once the maps library is loaded.
     *
     * @param {string} callback - The callback
     */
    addCallback: function (callback) {
      let self = this;
      // Ensure callbacks array.
      self.geoCallbacks = self.geoCallbacks || [];
      self.geoCallbacks.push({callback: callback});
    },

    /**
     * Load GEO API
     */
    load: function (mapid, settings, callback) {
      var self = this;

      // Add the callback.
      self.addCallback(callback);

      if (typeof geo === "undefined") {
        if (self.maps_api_loading === true) {
          return;
        }
        self.maps_api_loading = true;

        let geo_api_version = settings && settings.api_version ? settings.api_version : 'last';
        var scriptPath =
          "https://geoapi.business-geografic.com/api/" + geo_api_version + "/";
        console.log("Loading GEO API Script", scriptPath);
        // cache GEO API script
        // https://gist.github.com/steveosoule/628430dacde21fd766fe8c4e796dad94
        $.getScript({url: scriptPath, cache: true})
          .done(function () {
            self.maps_api_loading = false;
            self.mapCallback();
          })
          .fail(function () {
            $("#" + mapid).html(
              "Could not load GEO API with URL '" + scriptPath + "'"
            );
          });
      } else {
        // API loaded. Run callback.
        self.mapCallback();
      }
    },

    // Center the map to the marker position.
    find_marker: Drupal.geoFieldMap.find_marker,

    // Place marker at the current center of the map.
    place_marker: function (mapid) {
      let self = this;
      if (self.map_data[mapid].click_to_place_marker) {
        if (!window.confirm(Drupal.t('Change marker position ?'))) {
          return;
        }
      }
      var geoApp = self.map_data[mapid].map;
      var extent = geoApp.map.extent;
      var center = [
        extent.minX + (extent.maxX - extent.minX) / 2,
        extent.minY + (extent.maxY - extent.minY) / 2,
      ];
      geoApp
        .transform(center, null, "EPSG:4326")
        .subscribe(function (latLng) {
          let position = {
            lat: latLng[1],
            lng: latLng[0],
          };
          self.setMarkerPosition(mapid, position);
          self.geofields_update(mapid, position);
        });
    },

    // Remove marker from the map.
    remove_marker: function (mapid) {
      let self = this;
      if (self.map_data[mapid].click_to_remove_marker) {
        if (!window.confirm(Drupal.t('Remove marker from map ?'))) {
          return;
        }
      }
      let position = {
        lat: 0,
        lng: 0,
      };
      self.setMarkerPosition(mapid, position);
      $('#' + self.map_data[mapid].latid).val(null);
      $('#' + self.map_data[mapid].lngid).val(null);
    },

    // Geofields update.
    geofields_update: Drupal.geoFieldMap.geofields_update,

    // Onchange of Geofields.
    geofield_onchange: function (mapid) {
      let self = this;
      let position = {
        lat: parseFloat($("#" + self.map_data[mapid].latid).val()),
        lng: parseFloat($("#" + self.map_data[mapid].lngid).val()),
      };
      self.setMarkerPosition(mapid, position);
      let zoom = self.map_data[mapid].zoom_focus;
      self.mapSetCenterAndZoom(mapid, position, zoom);
      self.reverse_geocode(mapid, position);
    },

    // Coordinates update.
    setLatLngValues: function (mapid, position) {
      let self = this;
      $("#" + self.map_data[mapid].latid).val(position.lat.toFixed(6));
      $("#" + self.map_data[mapid].lngid).val(position.lng.toFixed(6));
    },

    // Set the Reverse Geocode result into the Client Side Storage.
    set_reverse_geocode_storage: Drupal.geoFieldMap.set_reverse_geocode_storage,

    // Get the Reverse Geocode result from Client Side Storage.
    get_reverse_geocode_storage: Drupal.geoFieldMap.get_reverse_geocode_storage,

    // Reverse geocode.
    reverse_geocode: function (mapid, position) {
      let self = this;
      let latlng = position.lat.toFixed(6) + "," + position.lng.toFixed(6);
      // Check the result from the chosen client side storage, and use it eventually.
      let reverse_geocode_storage = self.get_reverse_geocode_storage(mapid, latlng);
      if (localStorage && self.map_data[mapid].geocoder.caching.clientside && self.map_data[mapid].geocoder.caching.clientside !== '_none_' && reverse_geocode_storage !== null) {
        if (self.map_data[mapid].search) { // FM 2022-11-09: crash on geolocation onchange event
          self.map_data[mapid].search.val(reverse_geocode_storage);
          self.setGeoaddressField(mapid, reverse_geocode_storage);
        }
      } else if (self.map_data[mapid].gmap_geocoder === 1) {
        let providers = self.map_data[mapid].gmap_geocoder_settings.providers.toString();
        let options = self.map_data[mapid].gmap_geocoder_settings.options;
        Drupal.geoFieldMapGeocoder.reverse_geocode(latlng, providers, options).done(function (results, status, jqXHR) {
          if (status === 'success' && results[0]) {
            self.set_reverse_geocode_result(mapid, latlng, results[0].formatted_address)
          }
        });
      }
      return status;
    },

    // Write the Reverse Geocode result in the Search Input field, in the
    // Geoaddress-ed field and in the Localstorage.
    set_reverse_geocode_result: Drupal.geoFieldMap.set_reverse_geocode_result,

    // Triggers the Geocode on the Geofield Map Widget.
    trigger_geocode: function (mapid, position) {
      let self = this;
      self.setMarkerPosition(mapid, position);
      let zoom = self.map_data[mapid].zoom_focus;
      self.mapSetCenterAndZoom(mapid, position, zoom);
      self.setLatLngValues(mapid, position);
      self.setGeoaddressField(mapid, self.map_data[mapid].search.val());
    },

    // Define a Geographical point, from coordinates.
    getLatLng: function (mapid, lat, lng) {
      let latLng = {
        lat: lat,
        lng: lng,
      };
      return latLng;
    },

    // Returns the Map Bounds, in the specific Map Library format.
    getMapBounds: function (mapid, map_library) {
      // not used
    },

    // Initialize the Geofield Map object.
    initGeofieldMap: function (params, callback) {
      let self = this;
      console.log("map_initialize GEO", params);

      if (params.ext.geo && params.ext.geo.settings && params.ext.geo.settings.application_url) {

        var urlApp = params.ext.geo.settings.application_url;

        var options = {
          elementId: params.mapid,
          applicationId: urlApp,
          map: {
            markers: [],
          },
          components: {
            // visibilité des composants
            // COMMON
            Header: {visible: false},
            UserInfoHeader: {visible: false},
            BaseLayerSwitcher: {visible: false},
            MapScaleLine: {visible: true},
            MapBasicControls: {visible: true},
            MapGeolocation: {visible: true},
            // GP
            LayerControl: {visible: false},
            PrintLink: {visible: false},
            SidePanel: {visible: false},
            // PRO
            MapIdentifierTool: {visible: false},
            LayerQueryTool: {visible: false},
            RightPanel: {visible: false},
            FunctionsLauncher: {visible: false},
          },
        };

        console.log("GEO Map creation options", options);
        var geoApp = new geo.Application(options);
        geoApp.render();
        geoApp.on("initialized", function () {
          console.log("GEO map initialized");
          // add reference to the application
          self.map_data[params.mapid].map = geoApp;
          // add marker when map is init
          setTimeout(function () {
            callback(geoApp);
          }, 100);
        });

      } else {
        window.alert('GEO application URL not found!');
      }
    },

    setZoomToFocus: function (mapid) {
      // difficile de faire une correspondance avec le niveau de zoom "focus" pour une carte GEO car fonctionne
      // en échelle et pas en niveau de zoom.
      let self = this;
      let geoApp = self.map_data[mapid].map;
      let extent = geoApp.map.extent;
      let center = [
        extent.minX + (extent.maxX - extent.minX) / 2,
        extent.minY + (extent.maxY - extent.minY) / 2,
      ];
      geoApp
        .transform(center, null, "EPSG:4326")
        .subscribe(function (latLng) {
          let position = {
            lat: latLng[1],
            lng: latLng[0],
          };
          let zoom = self.map_data[mapid].zoom_focus;
          self.mapSetCenterAndZoom(mapid, position, zoom);
        });
    },

    setMarker: function (mapid, position) {
      let self = this;
      let marker = {};
      var geoApp = self.map_data[mapid].map;
      if (geoApp.map.markers["marker-admin"]) {
        geoApp.map.removeMarkers(["marker-admin"]);
        self.map_data[mapid].marker = null;
      }
      marker = {
        position: {
          coordinates: [position.lng, position.lat],
          crs: 4326,
        },
        id: "marker-admin",
        imageUrl: "public/canvas/images/marker-icon.png",
        positioning: "bottom-center",
        tooltip: {},
        size: {
          w: 25,
          h: 41,
        },
      };
      geoApp.map.addMarkers([marker]);
      // Define a Drupal.geofield_map marker self property.
      self.map_data[mapid].marker = marker;
      return marker;
    },

    setMarkerPosition: function (mapid, position) {
      let self = this;
      self.setMarker(mapid, position);
    },

    getMarkerPosition: function (mapid) {
      let self = this;
      let marker = self.map_data[mapid].marker;
      let coordinates = marker.position.coordinates;
      return {
        lat: coordinates[1],
        lng: coordinates[0],
      };
    },

    mapSetCenter: function (mapid, position) {
      let self = this;
      var center = {
        coordinates: [position.lng, position.lat],
        crs: 4326,
      };
      var geoApp = self.map_data[mapid].map;
      geoApp.map.centerOn(center);
    },

    mapSetCenterAndZoom: function (mapid, position, zoom) {
      let self = this;
      var geoApp = self.map_data[mapid].map;
      geoApp.map.centerOn({
        coordinates: [position.lng, position.lat],
        crs: 4326,
        scaleDenominator: 2 ** (29 - zoom),
      });
    },

    setGeoaddressField: Drupal.geoFieldMap.setGeoaddressField,

    map_refresh: function (mapid) {
      let self = this;
      setTimeout(function () {
        self.find_marker(mapid);
      }, 10);
    },

    // Init Geofield Map and its functions.
    map_initialize: function (params, context) {
      let self = this;
      $.noConflict();

      if (params.searchid !== null) {
        // Define the Geocoder Search Field Selector.
        self.map_data[params.mapid].search = $('#' + params.searchid);
      }

      // Define the Geoaddress Associated Field Selector, if set.
      if (params.geoaddress_field_id !== null) {
        self.map_data[params.mapid].geoaddress_field = $('#' + params.geoaddress_field_id);
      }

      // Define the Geofield Position.
      let position = self.getLatLng(params.mapid, params.lat, params.lng);
      self.map_data[params.mapid].position = position;

      // Intialize the Geofield Map object.
      self.initGeofieldMap(params, function (geoApp) {

        // Define a map self property, so other code can interact with it.
        self.map_data[params.mapid].map = geoApp;

        // Generate and Set/Place Marker Position.
        self.setMarker(params.mapid, position);

        // Set map center and zoom level
        if (position && position.lat != 0 && position.lng != 0) {
          let zoom_start = Number(params.entity_operation !== "edit" ? params.zoom_start : params.zoom_focus);
          self.mapSetCenterAndZoom(params.mapid, position, zoom_start);
        } else {
          // emprise par défaut de la carte
        }

        // add click handler
        geoApp.map.on("pointerClick", function (event) {
          geoApp
            .transform(event.coordinates, null, "EPSG:4326")
            .subscribe(function (latLng) {
              var clickPosition = {
                lat: latLng[1],
                lng: latLng[0],
              };
              self.setMarkerPosition(params.mapid, clickPosition);
              self.geofields_update(params.mapid, clickPosition);
            });
        });
      });

      // Add the Geocoder Control and Options, if requested/enabled, and supported.
      if (params['gmap_geocoder'] === 1) {
        self.map_data[params.mapid].gmap_geocoder = params['gmap_geocoder'];
        self.map_data[params.mapid].gmap_geocoder_settings = params['gmap_geocoder_settings'];
      }

      // Bind click to find_marker functionality.
      $('#' + self.map_data[params.mapid].click_to_find_marker_id).click(function (e) {
        e.preventDefault();
        self.find_marker(self.map_data[params.mapid].mapid);
      });

      // Bind click to place_marker functionality.
      $('#' + self.map_data[params.mapid].click_to_place_marker_id).click(function (e) {
        e.preventDefault();
        self.place_marker(self.map_data[params.mapid].mapid);
      });

      // Bind click to remove_marker functionality.
      $('#' + self.map_data[params.mapid].click_to_remove_marker_id).click(function (e) {
        e.preventDefault();
        self.remove_marker(self.map_data[params.mapid].mapid);
      });

      // Define Lat & Lng input selectors and all related functionalities and Geofield Map Listeners.
      if (params.widget && params.latid && params.lngid) {

        // If it is defined the Geocode address Search field (dependant on the Gmaps API key)
        if (self.map_data[params.mapid].search) {

          if (Drupal.geoFieldMapGeocoder && self.map_data[params.mapid].gmap_geocoder === 1) {
            Drupal.geoFieldMapGeocoder.map_control_autocomplete(params.mapid, self.map_data[params.mapid].gmap_geocoder_settings, self.map_data[params.mapid].search, 'widget', params.map_library);
          }

          // Geocode user input on enter.
          self.map_data[params.mapid].search.keydown(function (e) {
            if (e.which === 13) {
              e.preventDefault();
              let input = self.map_data[params.mapid].search.val();
              // Execute the geocoder.
              Drupal.geoFieldMapGeocoder.geocode({address: input}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK && results[0]) {
                  // Triggers the Geocode on the Geofield Map Widget.
                  let position = self.getLatLng(params.mapid, results[0].geometry.location.lat(), results[0].geometry.location.lng());
                  self.trigger_geocode(params.mapid, position);
                }
              });
            }
          });
        }

        // Events on Lat field change.
        $('#' + self.map_data[params.mapid].latid).on('change', function (e) {
          self.geofield_onchange(params.mapid);
        }).keydown(function (e) {
          if (e.which === 13) {
            e.preventDefault();
            self.geofield_onchange(params.mapid);
          }
        });

        // Events on Lon field change.
        $('#' + self.map_data[params.mapid].lngid).on('change', function (e) {
          self.geofield_onchange(params.mapid);
        }).keydown(function (e) {
          if (e.which === 13) {
            e.preventDefault();
            self.geofield_onchange(params.mapid);
          }
        });

        // Set default search field value (just to the first geofield_map).
        if (self.map_data[params.mapid].search && self.map_data[params.mapid].geoaddress_field && !!self.map_data[params.mapid].geoaddress_field.val()) {
          // Copy from the geoaddress_field.val.
          self.map_data[params.mapid].search.val(self.map_data[params.mapid].geoaddress_field.val());
        }
        // If the coordinates are valid, provide a Gmap Reverse Geocode.
        else if (self.map_data[params.mapid].search && (Math.abs(params.lat) > 0 && Math.abs(params.lng) > 0)) {
          // The following will work only if a geocoder has been defined.
          self.reverse_geocode(params.mapid, position);
        }
      }

      // Trigger a custom event on Geofield Map initialized, with mapid.
      $(context).trigger('geofieldMapInit', params.mapid);
    }
  };

})(jQuery, Drupal, drupalSettings);
