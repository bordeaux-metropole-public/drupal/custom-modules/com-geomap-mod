# Module Drupal "Geofield Map Formatters and Widget for the GEO Software platform"

Ce module Drupal permet d'afficher des contenus cartographiques Drupal sur une
carte GEO.
Il s'utilise avec les modules ["geofield"](https://www.drupal.org/project/geofield)
et ["geofield_map"](https://www.drupal.org/project/geofield_map)
qui permettent d'enregistrer des coordonnées latitude/longitude dans un contenu
Drupal et de les afficher sur une carte.
Côté GEO, il est nécessaire d'avoir la licence "GEO API" pour pouvoir utiliser
ce module.
