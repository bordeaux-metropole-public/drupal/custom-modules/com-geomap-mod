<?php

namespace Drupal\geofield_map_geosoftware\Plugin\MapLibrary;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\geofield_map_ext\MapLibraryPluginBase;
use Drupal\geofield_map_ext\Plugin\Field\FieldWidget\GeofieldMapExtWidget;
use Drupal\geofield_map_geosoftware\GeoSoftwareGeofieldMapFieldUtils;

/**
 * Provides a GEO Software Map Library Plugin.
 *
 * @MapLibraryPlugin(
 *   id = "geo",
 *   label = "GEO Software Map Library",
 * )
 */
class GeoSoftwareMapLibraryPlugin extends MapLibraryPluginBase {

  /**
   * @inheritDoc
   */
  public function mapWidgetDefaultSettings(array &$default_settings) {
    $default_settings['geo_settings'] = [
      'application_url' => '',
      'api_version' => '',
    ];
  }

  /**
   * @inheritDoc
   */
  public function mapWidgetSettingsForm(GeofieldMapExtWidget $widget, array &$elements) {
    $settings = $widget->getSettings();

    // Add GEO to library selector.
    $elements['map_library']['#options'][$this->getPluginId()] = $this->t('GEO Software');

    // Set Geo Settings Element.
    GeoSoftwareGeofieldMapFieldUtils::setGeoSettingsElement($widget, $settings, $elements, TRUE);

    // Hide map_type_selector when GEO library is selected.
    $elements['map_type_selector']['#states'] = [
      'invisible' => [
        ':input[name="fields[' . $widget->getFieldName() . '][settings_edit_form][settings][map_library]"]'
        => ['value' => $this->getPluginId()],
      ],
    ];
  }

  /**
   * @inheritDoc
   */
  public function mapWidgetSettingsSummary(GeofieldMapExtWidget $widget, array &$summary) {
    $settings = $widget->getSettings();

    $summary['map_library'] = [
      '#markup' => $this->t('Map Library: @state', ['@state' => $this->t('GEO Software Map')]),
    ];
    unset($summary['map_type']);
    unset($summary['map_type_selector']);

    $summary['geo_settings'] =
      GeoSoftwareGeofieldMapFieldUtils::setGeoSettingsSummaryElement($settings['geo_settings'] ?? []);
  }

  /**
   * @inheritDoc
   */
  public function mapWidgetFormElement(GeofieldMapExtWidget $widget, array &$element) {
    $element['#geo_settings'] = $widget->getSetting('geo_settings') ?? [];
  }

  /**
   * @inheritDoc
   */
  public function mapFormElement(array &$element) {
    $map_library = $this->getPluginId();

    $element['#attached']['library'][] = 'geofield_map_geosoftware/geofield_geosoftware_ext_map_widget';

    $maps_settings = $element['#attached']['drupalSettings']['geofield_map'];
    $maps_settings[$element['#mapid']]['ext'][$map_library]['settings'] = $element['#geo_settings'];
    $element['#attached']['drupalSettings']['geofield_map'] = $maps_settings;
  }

  /**
   * @inheritDoc
   */
  public function mapWidgetWebformSettings(&$elements) {

    $elements['map_library']['#options'][$this->getPluginId()] = $this->t('GEO Software Map');

    $library_geo_states = [
      'visible' => [':input[name="properties[map_library]"]' => ['value' => $this->getPluginId()]],
      'required' => [':input[name="properties[map_library]"]' => ['value' => $this->getPluginId()]],
    ];

    // GEO specific fields.
    $elements['geo_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('GEO Settings'),
      '#states' => $library_geo_states,
    ];
    $elements['geo_settings']['geo_application_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GEO Application URL'),
      // '#default_value' =>
      // $this->getSetting('geo_settings')['application_url'],
      '#description' => $this->t('URL of your GEO application. The application must contain the "GEO API v2" module.'),
      '#placeholder' => $this->t('https://geo.com/adws/app/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx/'),
      '#states' => $library_geo_states,
      '#access' => TRUE,
    ];
    $elements['geo_settings']['geo_api_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GEO API Version'),
      // '#default_value' => $this->getSetting('geo_settings')['api_version'],
      '#description' => $this->t('Check that the GEO API version is compatible with your GEO version.<br>@geo_api_version_link.', [
        '@geo_api_version_link' => Link::fromTextAndUrl(t('Check the compatibility matrix in the GEO API documentation'),
          Url::fromUri('https://docgeoapi.business-geografic.com/en/guide/1-Overview/requirements', [
            'absolute' => TRUE,
            'attributes' => ['target' => '_blank'],
          ]))->toString(),
      ]),
      '#placeholder' => $this->t('v2.2.2'),
      '#states' => $library_geo_states,
      '#access' => TRUE,
    ];
  }

  /**
   * @inheritDoc
   */
  public function mapWidgetWebformElement(&$element) {
    if (isset($element['#geo_application_url'])) {
      $element['#geo_settings'] = [
        'application_url' => $element['#geo_application_url'],
        'api_version' => $element['#geo_api_version'] ?? 'last',
      ];
    }
    else {
      $element['#geo_settings'] = [];
    }
    unset($element['#geo_application_url']);
    unset($element['#geo_api_version']);
  }

}
