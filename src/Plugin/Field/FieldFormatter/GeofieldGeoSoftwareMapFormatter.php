<?php

namespace Drupal\geofield_map_geosoftware\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\geofield_map\Plugin\Field\FieldFormatter\GeofieldGoogleMapFormatter;
use Drupal\geofield_map_ext\GeofieldMapFieldInterface;
use Drupal\geofield_map_geosoftware\GeoSoftwareGeofieldMapFieldTrait;

/**
 * Plugin implementation of the 'geofield_geosoftware_map' formatter.
 *
 * @FieldFormatter(
 *   id = "geofield_geosoftware_map",
 *   label = @Translation("GEO Software Map"),
 *   field_types = {
 *     "geofield"
 *   }
 * )
 */
class GeofieldGeoSoftwareMapFormatter extends GeofieldGoogleMapFormatter implements GeofieldMapFieldInterface {

  use GeoSoftwareGeofieldMapFieldTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return self::getGeoSoftwareDefaultSettings() + parent::defaultSettings();
  }

  /**
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   */
  public function getFieldName() {
    return $this->fieldDefinition->getName();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $settings = $this->getSettings();

    $summary['geo_settings'] = $this->setGeoSettingsSummaryElement($settings['geo_settings']);

    /* Fixes crash in geofield_map when switching from old geosoftware format */
    if (!$settings['map_controls']['map_type_control_options_type_ids']) {
      $map_controls = $settings['map_controls'];
      $map_controls['map_type_control_options_type_ids'] = [];
      $this->setSetting('map_controls', $map_controls);
    }

    $parent_summary = parent::settingsSummary();
    unset($parent_summary['map_google_api_key']);
    unset($parent_summary['map_controls']);
    unset($parent_summary['map_oms']);
    unset($parent_summary['custom_style_map']);

    return $summary + $parent_summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element_array = parent::viewElements($items, $langcode);

    if (isset($element_array[0])) {

      $element = $element_array[0];

      if (isset($element['#attached']['library'])) {
        $element['#attached']['library'] =
          array_diff($element['#attached']['library'],
            [
              'geofield_map/geofield_google_map',
              'geofield_map/overlappingmarkerspiderfier',
            ]);
      }
      else {
        $element['#attached']['library'] = [];
      }

      $element['#attached']['library'][] = 'geofield_map_geosoftware/geofield_geosoftware_map';

      $element['#theme'] = 'geofield_geosoftware_map';

      if (isset($element['#attached']['drupalSettings'])) {
        $drupalSettings = $element['#attached']['drupalSettings']['geofield_google_map'];
        $element['#attached']['drupalSettings']['geofield_geosoftware_map'] = $drupalSettings;
        unset($element['#attached']['drupalSettings']['geofield_google_map']);
      }

      $element_array[0] = $element;
    }

    return $element_array;
  }

}
