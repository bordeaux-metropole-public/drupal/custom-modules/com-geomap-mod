<?php

namespace Drupal\geofield_map_geosoftware;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\geofield_map\GeofieldMapFieldTrait;

/**
 * Class GeoSoftwareGeofieldMapFieldTrait.
 *
 * Provide common functions for Geofield GEO Software Map fields.
 *
 * @package Drupal\geofield_map
 */
trait GeoSoftwareGeofieldMapFieldTrait {

  use GeofieldMapFieldTrait;

  /**
   * Get the GEO Software Default Settings.
   *
   * @return array
   *   The default settings.
   */
  public static function getGeoSoftwareDefaultSettings() {
    return [
      'geo_settings' => [
        'application_url' => '',
        'api_version' => '',
      ],
    ];
  }

  /**
   * @return \Drupal\Core\Utility\LinkGeneratorInterface
   */
  public function getLink() {
    return $this->link;
  }

  /**
   * Set Map GEO Settings Summary Element.
   */
  protected function setGeoSettingsSummaryElement(array $geo_settings) {
    return GeoSoftwareGeofieldMapFieldUtils::setGeoSettingsSummaryElement($geo_settings);
  }

  /**
   * Pre Process the MapSettings.
   *
   * Performs some preprocess on the maps settings before sending to js.
   *
   * @param array $map_settings
   *   The map settings.
   */
  protected function preProcessMapSettings(array &$map_settings): void {
    // Generate Absolute icon_image_path, if it is not.
    $icon_image_path = $map_settings['map_marker_and_infowindow']['icon_image_path'];
    if (!empty($icon_image_path) && !UrlHelper::isExternal($map_settings['map_marker_and_infowindow']['icon_image_path'])) {
      $map_settings['map_marker_and_infowindow']['icon_image_path'] = Url::fromUri('base:', ['absolute' => TRUE])->toString() . $icon_image_path;
    }
  }

  /**
   * Generate the GEO Map Settings Form.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $settings
   *   Form settings.
   * @param array $default_settings
   *   Default settings.
   *
   * @return array
   *   The GMap Settings Form
   */
  public function generateGmapSettingsForm(
    array $form,
  FormStateInterface $form_state,
  array $settings,
  array $default_settings) {

    $elements['#attached'] = [
      'library' => [
        'geofield_map/geofield_map_view_display_settings',
      ],
    ];

    $elements = [];

    // Attach Geofield Map Library.
    $elements['#attached']['library'] = [
      'geofield_map/geofield_map_general',
    ];

    // Set Geo Settings Element.
    GeoSoftwareGeofieldMapFieldUtils::setGeoSettingsElement($this, $settings, $elements);

    // Set Map Dimension Element.
    $this->setMapDimensionsElement($settings, $elements);

    // Set Map Empty Options Element.
    $this->setMapEmptyElement($settings, $elements);

    // Set Map Center Element.
    $this->setMapCenterElement($settings, $elements);

    // Set Map Zoom and Pan Element.
    $this->setMapZoomAndPanElement($settings, $default_settings, $elements);

    // Set Map Control Element.
    // $this->setMapControlsElement($settings, $elements);
    // Set Map Marker and Infowindow Element.
    $this->setMapMarkerAndInfowindowElement($form, $settings, $elements);

    // Set Map Additional Options Element.
    $this->setMapAdditionalOptionsElement($settings, $elements);
    // Set Map Geometries Options Element.
    // $this->setGeometriesAdditionalOptionsElement($settings, $elements);
    // Set Overlapping Marker Spiderfier Element.
    // $this->setMapOmsElement($settings, $default_settings, $elements);
    // Set Custom Map Style Element.
    // $this->setCustomStyleMapElement($settings, $elements);
    // Set Map Marker Cluster Element.
    $this->setMapMarkerclusterElement($settings, $elements);

    // Set Map Geocoder Control Element, if the Geocoder Module exists,
    // otherwise output a tip on Geocoder Module Integration.
    $this->setGeocoderMapControl($elements, $settings);

    // Set Map Lazy Load Element.
    $this->setMapLazyLoad($settings, $elements);

    return $elements;

  }

}
